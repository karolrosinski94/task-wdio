import { HeaderPage } from "../../pages/HeaderPage.js";
import { MainPage } from "../../pages/MainPage.js";
import { WorkspacePage } from "../../pages/WorkspacePage.js";
import { login } from "./shared/shared.js";

describe("Workspace-operations", () => {
    const mainPage = new MainPage();
    const workspacePage = new WorkspacePage();
    const headerPage = new HeaderPage();

    beforeEach(async () => {
        await login();
    });

    it("User changes workspace’s name", async () => { 
        await browser.waitUntil(
            async function () {
                return await mainPage.boardBadgeDivt.waitForExist({ timeout: 5000 });
            },
            5000,
            "custom error",
            1000
        );
        await headerPage.workspaceSwitchBtn.click();
        await headerPage.workspaceOptionBtn.click();

        await workspacePage.editNameBtn.click();
        await workspacePage.editNameInput.setValue("new-workspace");
        await workspacePage.editNameConfirmBtn.click();

        await expect(workspacePage.workspaceNameText).toHaveText("new-workspace");
    });
});
