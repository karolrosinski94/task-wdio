import axios from "axios";
import { expect } from "chai";

const apiUrl = "https://api.dropboxapi.com";
const contentUrl = "https://content.dropboxapi.com";
const token =
    "uat.AE1x1D0BhiQls2Y4feLBaNq6nURxvz0iAuk-Lc7Aqu-y012TRu6DKCNQbqJQvTLoaqVpJej933KhKmQuf9rQXH4xmm8jspaQ9w1bnqEDHoZ0ssFU7Qw814R3JdwhaCV07j-iIbPXoTYT4b9dwSL0hgIcSTuS0BqrWWvG27s_VhjDKlv0x-q4dy_TEQBGwiOnGsYndWBZ9oppnOPnn88IrK41xGpIAPRK7rFHAqytpWzggudPHzL1PMHNwQpHeL9PNMfNR7_Pl0ayOSsDHT89K6031khADtBykMl65vMSOzoqmBboZWPLw6xEBW1qGicQI85xllAan4WWVimqcOpGb7OIWl0K48pjx21yUNac_53c20HxMsCY1czHI1g72naw9yVAyUmSPAUorFYL88QSrlhXq-RGOZYoIP27G-CMeWx5ICH7ednLzA-ivva2mVbxD9fZmfzJz66Rlm6JnGPYiRsDa-jYD2lgRPncexBS8YTs5K2AswXCMxp2Qab5wXfPjVPKV62N-o8N0AOXKpXx_JclO1PfTj5UADj0CUS3fjL9kOKDHu5Gt-oxVG38X3aN1iG_874naN8VOGHrIKIg24egTWnCtjxmkY1J-VHihf_N0n2Gc9N01C7o4LeLBLKTm-sntuQB4jyi-6YyYiKbSd4HHdK3OpJQGHDfhYgX9n2eLn_T68jaEGHMg2r2nFevMylL4AiMtsIrDHcJNKX1beZrBHbpclH7xP3WGYW10FqcPipz0Pu-paiCD2PLkUEH5-yujJuQMwt_PoRbE5c_IFGIkde578LstP7qM61b8G6oU6i-Bjsyox0JVRj9lgy5XGzD30TS0K_jj3XGGvW-0B9dK4Skw7K26FXM5vQvD2YGX-uZXmz6FwLoPwBn7Oz6Tyu7bXUjf_DUHfv8DMTvT7vcIfQJ2U6o96acn1GvMMJYINVZI9aNeynK1BaEZL5hB_QR9rtKdUv6VDjeDmkHAN3dU2Q18em8zLs0d22-gzCKC7waKyDrDKSsgR9W6ZkHsHOojgqMBh-S-rU3wcEaNdqGzlD5PVY9hRjbaVOLQhl0qmtwRqFx9jLFDAYynAECi8dPwOiUf1kUhJDBtDqgUGtDi-1KKdhKBEIj0jFvZqmyWdTHfWxl4jLHbD2qDmmpK26KYF6Y7JlYzPiIEB_0vCVtew8qOVt13okV36rf6DENsVgLvFoy08VEhTFi9LWSrXx077qfvx45zcueitwRVchg66CHDABk_wE7thjVrd5cAkI5cTSKCHyLbOER-DC7EP3ju1tbFxvYUUQjLt58ZHwCYlqdlMAVdT0K8FKZRXA1rHFoUv2IVOwai18bixTGkKnDSt6YUq6nDwtr8LCpqvPdPNof4IlDaLEQ1EHohAHE5Vbhufy-OuboNFguE02jNcAO4stssSsXY31HFBouTvj_";
const configApi = {
    headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
    },
};
const configContent = {
    headers: {
        Authorization: `Bearer ${token}`,
        "Dropbox-API-Arg": '{"path":"/new/test-upload.txt"}',
        "Content-Type": "application/octet-stream",
    },
};

const form = new FormData();

describe("Dropbox API tests", function () {
    it("Test connection - /check/user endpoint should have 200 status", async function () {
        try {
            const response = await axios.post(
                apiUrl + "/2/check/user",
                { query: "test" },
                configApi
            );
            expect(response.status).to.be.equal(200);
        } catch (error) {
            throw error;
        }
    });

    it("Test connection - /check/user endpoint should return correct data", async function () {
        try {
            const response = await axios.post(
                apiUrl + "/2/check/user",
                { query: "test" },
                configApi
            );
            expect(response.data).to.be.deep.equal({ result: "test" });
        } catch (error) {
            throw error;
        }
    });

    it("test file upload - should return name and extension of file uploaded", async function () {
        try {
            const response = await axios.post(
                contentUrl + "/2/files/upload",
                form.append("test-upload", new Blob(["text"])),
                configContent
            );
            expect(response.data.name).to.be.equal("test-upload.txt");
        } catch (error) {
            throw error;
        }
    });

    it("test file upload should return status 200 and have headers", async function () {
        try {
            const response = await axios.post(
                contentUrl + "/2/files/upload",
                form.append("test-upload", new Blob(["text"])),
                configContent
            );
            expect(response.status).to.be.equal(200);
            expect(response.headers).to.exist;
        } catch (error) {
            throw error;
        }
    });

    it("get_metadata should return correct tag 'file'", async function () {
        try {
            const response = await axios.post(
                apiUrl + "/2/files/get_metadata",
                { path: "/new/test-upload.txt" },
                configApi
            );
            expect(response.data[".tag"]).to.be.equal("file");
        } catch (error) {
            throw error;
        }
    });

    it("get_metadata should return 200 and have headers", async function () {
        try {
            const response = await axios.post(
                apiUrl + "/2/files/get_metadata",
                { path: "/new/test-upload.txt" },
                configApi
            );
            expect(response.status).to.be.equal(200);
            expect(response.headers).to.exist;
        } catch (error) {
            throw error;
        }
    });

    it("delete file should return metadata with name of the file, status code 200, headers should exist", async function () {
        try {
            const response = await axios.post(
                apiUrl + "/2/files/delete_v2",
                { path: "/new/test-upload.txt" },
                configApi
            );
            expect(response.data.metadata.name).to.be.equal("test-upload.txt");
            expect(response.status).to.be.equal(200);
            expect(response.headers).to.exist;
        } catch (error) {
            throw error;
        }
    });
});
