import { LoginPage } from "../../../pages/LoginPage.js";
import { TRELO_LOGIN, TRELO_PASS } from "./constants.js";

export async function login() {
    const loginPage = new LoginPage();

    await browser.url('/');
    await browser.maximizeWindow();
    await loginPage.loginBtn.click()

    await loginPage.usernameInput.setValue(TRELO_LOGIN)
    await loginPage.loginBtnInside.click()
    await loginPage.passwordInput.waitForDisplayed({ timeout: 3000 });
    await loginPage.passwordInput.setValue(TRELO_PASS);
    await loginPage.loginBtnInside.click();
}
