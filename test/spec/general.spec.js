import { assert, expect } from "chai";
import { login } from "./shared/shared.js";
import * as chai from "chai";
import { MainPage } from "../../pages/MainPage.js";
import { HeaderPage } from "../../pages/HeaderPage.js";

describe("General tests across trello workspace", () => {
    const mainPage = new MainPage();
    const headerPage = new HeaderPage();

    beforeEach(async function () {
        await login();
    });

    it("clicking Logo should take User to boards view", async function () {
        await browser.waitUntil(
            async function () {
                return await headerPage.logo.waitForExist({
                    timeout: 5000,
                });
            },
            5000,
            "custom error",
            1000
        );

        await headerPage.logo.click();

        await assert.equal(
            await browser.getUrl(),
            "https://trello.com/u/supertoster1/boards"
        );
    });

    it("Clicking templates should reveal propositions in the center", async function () {
        await mainPage.templatesOpt.click();
        await browser.waitUntil(async function () {
            return await mainPage.templatesList[0]
                .waitForExist({ timeout: 5000 });
        });

        chai.should(mainPage.templatesList).exist
    });

    it("Boards option in side navbar should be selected after login", async function () {
        await browser.waitUntil(async function () {
            return await mainPage.boardsOpt.waitForExist({
                timeout: 5000,
            });
        });
        expect(await mainPage.boardsOpt.getAttribute("data-selected")).to.be.equal('true');
    });

    afterEach(async function() {
        await browser.reloadSession();
    })
});
