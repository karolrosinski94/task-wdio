import { BoardPage } from "../../pages/BoardPage.js";
import { HeaderPage } from "../../pages/HeaderPage.js";
import { MainPage } from "../../pages/MainPage.js";
import { login } from "./shared/shared.js";

describe("Board operations", async () => {
    const headerPage = new HeaderPage();
    const mainPage = new MainPage();
    const boardPage = new BoardPage();

    beforeEach(async () => {
        await login();
    });

    it("User correctly creates new Board", async () => {
        await headerPage.createBtn.click();
        await headerPage.createBoardBtn.click({ skipRelease: true });
        await headerPage.titleBoardInput.setValue("New board");
        await headerPage.createBoardBtnInside.click();

        await expect(boardPage.boardTitle).toExist()
    });

    it("User creates list in their Board", async () => {
        await browser.waitUntil(
            async function () {
                return await mainPage.boardTestBadge[0].waitForExist({
                    timeout: 5000,
                });
            },
            5000,
            "custom error",
            1000
        );
        await mainPage.boardTestBadge[0].click();
        
        await browser.waitUntil(
            async function () {
                return await boardPage.addListBtn.waitForExist({
                    timeout: 5000,
                });
            },
            5000,
            "custom error",
            1000
        );

        await boardPage.addListBtn.click();
        await boardPage.listNameInput.setValue("My list");
        await boardPage.addListConfirmBtn.click();

        await expect(boardPage.listTitleH2.at(-1)).toHaveText("My list");
    });

    it("User creates card in their Board", async () => {
        await browser.waitUntil(
            async function () {
                return await mainPage.boardCardDiv.waitForExist({
                    timeout: 5000,
                });
            },
            5000,
            "custom error",
            1000
        );
        await mainPage.boardCardDiv.click();

        await boardPage.addCardBtn.click();
        await boardPage.cardTextArea.setValue("My card");
        await boardPage.cardConfirmBtn.click();
        await expect(boardPage.cardTextAnchor.at(-1)).toHaveText(
            "My card"
        );
    });

    it("User sorts cards in their Board by newest first", async () => {
        await browser.waitUntil(
            async function () {
                return await mainPage.boardBadgeDivt.waitForExist({ timeout: 5000 });
            },
            5000,
            "custom error",
            1000
        );
        await mainPage.boardBadgeDivt.click();

        await boardPage.listEditBtn.click();
        await boardPage.sortBtn.click();
        await boardPage.sortByNewestBtn.click();

        await expect(boardPage.cardNameText).toHaveText("Sort Card 2");
    });

    afterEach(async () => {
        await browser.reloadSession();
    });
});
