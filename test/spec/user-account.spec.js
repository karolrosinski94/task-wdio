import { login } from "./shared/shared.js";
import { TEMP_EMAIL, TEMP_EMAIL_PASS, TEMP_URL } from "./shared/constants.js";
import { EmailPage } from "../../pages/EmailPage.js";
import { TestEmailPage } from "../../pages/TestEmailPage.js";
import { config } from "../../wdio.conf.js";

describe("User account", async () => {
    const emailPage = new EmailPage();
    const testEmailPage = new TestEmailPage();

    //Skipped - test email is no longer valid
    it.skip("User gets sent an email with verification link during Signing up", async () => {
        await browser.url("/");
        await browser.maximizeWindow();

        await emailPage.signUpButton.click();
        await emailPage.emailInput.setValue(TEMP_EMAIL);
        await emailPage.signUpButtonInside.click();
        /*
            waits 5 seconds for verification link to be sent
        */
        await browser.pause(5000);

        await browser.url(TEMP_URL);
        await testEmailPage.emailInputMail.setValue(TEMP_EMAIL);
        await testEmailPage.passwordInput.setValue(TEMP_EMAIL_PASS);
        await testEmailPage.confrimBtn.click();

        await browser.waitUntil(async function () {
            return testEmailPage.emailNameText.waitForExist();
        });

        await expect(testEmailPage.emailNameText).toHaveText("Trello");
    });

    it("User successfully signs in using trello account", async () => {
        await login();

        await expect(browser).toHaveUrl(
            config.baseUrl + "u/supertoster1/boards"
        );
    });
});
