import { HeaderPage } from "../../pages/HeaderPage.js";
import { ProfileSettingsPage } from "../../pages/ProfileSettingsPage.js";
import { login } from "./shared/shared.js";

describe("Profile settings", async () => {
    const headerPage = new HeaderPage();
    const profileSettingsPage = new ProfileSettingsPage();

    beforeEach(async () => {
        await login();
    });

    it("User correctly changes their Bio", async () => {
        await headerPage.avatarBtn.click();
        await headerPage.profileBtn.waitForDisplayed();
        await headerPage.profileBtn.click();

        await profileSettingsPage.bioInput.setValue("This is my bio");
        await profileSettingsPage.submitBtn.click();

        await browser.refresh();

        await expect(profileSettingsPage.bioInput).toHaveValue(
            "This is my bio",
            {
                ignoreCase: false,
            }
        );
    });
});
