import { BasePage } from "./BasePage.js";

export class EmailPage extends BasePage {
    get signUpButton() {
        return browser.$('[data-uuid="MJFtCCgVhXrVl7v9HA7EH_signup"]');
    }

    get emailInput() {
        return browser.$("#email");
    }

    get signUpButtonInside() {
        return browser.$("#signup-submit");
    }
}
