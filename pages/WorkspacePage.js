import { BasePage } from "./BasePage.js";

export class WorkspacePage extends BasePage {
    get editNameBtn() {
        return browser.$('[data-testid="EditIcon"]');
    }

    get editNameInput() {
        return browser.$("#displayName");
    }

    get editNameConfirmBtn() {
        return browser.$('.js-current-details [type="submit"]');;
    }
    
    get workspaceNameText() {
        return browser.$(".js-current-details h2");
    }
}