import { BasePage } from "./BasePage.js";

export class TestEmailPage extends BasePage {
    get emailInputMail() {
        return browser.$("#v20-input");
    }

    get passwordInput() {
        return browser.$("#v21-input");
    }

    get confrimBtn() {
        return browser.$("#v23");
    }

    get emailNameText() {
      return browser.$(".v-MailboxItem-name span");
    }
}
