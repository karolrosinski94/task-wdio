import { BasePage } from "./BasePage.js";

export class MainPage extends BasePage {
    get boardTitleH1() {
        return browser.$(".board-tile-details-name > div");
    }

    get boardBadgeDiv() {
        return browser.$('[title="New board"]');
    }

    get boardTestBadge() {
        return browser.$$('[title="test-list"]');
    }

    get boardBadgeDivt() {
        return browser.$('[title="Sort Board"]');
    }

    get boardCardDiv() {
        return browser.$('div[title="aaaaa"]');
    }

    get templatesOpt() {
        return browser.$('[data-testid="templates"]');
    }

    get templatesList() {
        return $$('[data-testid="templates-container"] a[title]');
    }

    get boardsOpt() {
        return browser.$(
            ".home-left-sidebar-container > div > ul > li:first-child a"
        );
    }
}
