import { BasePage } from "./BasePage.js";

export class ProfileSettingsPage extends BasePage {
    get bioInput() {
        return browser.$("#bio");
    }

    get submitBtn() {
        return browser.$('[data-testid="profile-form"] > button');
    }
}