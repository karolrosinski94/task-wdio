import { BasePage } from "./BasePage.js";

export class HeaderPage extends BasePage {
    get avatarBtn() {
        return browser.$('[data-testid="header-member-menu-button"]');
    }

    get profileBtn() {
        return browser.$('[data-testid="account-menu-profile"]');
    }

    get createBtn() {
        return browser.$('[data-testid="header-create-menu-button"]');
    }

    get createBoardBtn() {
        return browser.$('[data-testid="header-create-board-button"]');
    }

    get titleBoardInput() {
        return browser.$('[data-testid="create-board-title-input"]');
    }

    get createBoardBtnInside() {
        return browser.$('[data-testid="create-board-submit-button"]');
    }

    get workspaceSwitchBtn() {
        return browser.$('[data-testid="workspace-switcher"]');
    }

    get workspaceOptionBtn() {
        return browser.$('[data-testid="workspace-switcher-popover-tile"]');
    }

    get logo() {
        return browser.$('nav > a[href="/"]');;
    }
}
