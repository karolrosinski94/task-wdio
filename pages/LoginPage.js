import { BasePage } from "./BasePage.js";

export class LoginPage extends BasePage {
    get loginBtn() {
        return browser.$('[data-uuid="MJFtCCgVhXrVl7v9HA7EH_login"]');
    }

    get usernameInput() {
        return browser.$("#username");
    }

    get loginBtnInside() {
        return browser.$("#login-submit");
    }

    get passwordInput() {
        return browser.$("#password");
    }
}

