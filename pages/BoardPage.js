import { BasePage } from "./BasePage.js";

export class BoardPage extends BasePage {
    get addListBtn() {
        return browser.$('[data-testid="list-composer-button"]');
    }

    get boardTitle() {
        return browser.$('[data-testid="board-name-display"]');
    }

    get listNameInput() {
        return browser.$("textarea[placeholder]");
    }

    get addListConfirmBtn() {
        return browser.$('[data-testid="list-composer-add-list-button"]');
    }

    get listTitleH2() {
        return browser.$$('[data-testid="list-name"]');
    }

    get addCardBtn() {
        return browser.$('[data-testid="list-add-card-button"]');
    }
    
    get cardTextArea() {
        return browser.$('[data-testid="list-card-composer-textarea"]');
    }

    get cardConfirmBtn() {
        return browser.$('[data-testid="list-card-composer-add-card-button"]');
    }

    get cardTextAnchor() {
        return browser.$$('[data-testid="card-name"]');
    }

    get listEditBtn() {
        return browser.$('[data-testid="list-edit-menu-button"]');
    }

    get sortBtn() {
        return browser.$(".js-sort-cards");
    }

    get sortByNewestBtn() {
        return browser.$(".js-sort-newest-first");
    }

    get cardNameText() {
        return browser.$$('[data-testid="card-name"]')[0];
    }
}
